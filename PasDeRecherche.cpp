// Ceci n'est pas un arbre de recherche.

#include <iostream>
#include <vector>
using namespace std;

struct noeud {
    int val;
    noeud * gauche;
    noeud * droite;
};

class Arbre
{
    public:
        Arbre()
        {
            racine = nullptr;
        }

        noeud * creerFeuille(int val)
        {
            noeud * n = new noeud;
            n->val = val;
            n->gauche = nullptr;
            n->droite = nullptr;

            return n;
        }

        void ajouterFeuille(int val) //n devient la racine, un ptr qui pointera vers le nouveau noeud a ajouter.
        {
            ajouterFeuillePrivate(val, racine);
        }

        void affichageInfixe()
        {
            affichageInfixePrivate(racine);

        }
        void affichagePrefixe()
        {
            affichagePrefixePrivate(racine);

        }
        void affichageSuffixe()
        {
            affichageSuffixePrivate(racine);

        }

        bool estUnABR()
        {
            return estABR(racine);
        }


    private:
        int cptg = 0;
        int cptd = 0;
        bool gaucheVide = true;
        void ajouterFeuillePrivate(int val, noeud* n) 
        {

            if (racine == nullptr)
            {
                racine = creerFeuille(val);
            }
            else if (gaucheVide)
            {
                if (n->gauche != nullptr) 
                {
                    ajouterFeuillePrivate(val, n->gauche);
                }
                else
                {
                    n->gauche = creerFeuille(val);
                    ++cptg;
                    gaucheVide = false;
                }

            }
            else
            {
                if (n->droite != nullptr)
                {
                    ajouterFeuillePrivate(val, n->droite);
                }
                else
                {
                    n->droite = creerFeuille(val);
                    ++cptd;
                }
                gaucheVide = true;

            }
            cout << "g:  " << cptg << " dr:  " << cptd << endl;
        }

        void affichageInfixePrivate(noeud* n) //LDR, Left then data then right.
        {
            if (n == nullptr)
            {
                return;
            }

            affichageInfixePrivate(n->gauche);
            cout << n->val << " ";
            affichageInfixePrivate(n->droite);
        }
        noeud * racine;

        void affichagePrefixePrivate(noeud* n) //DLR, Data then left then right
        {
            if (n == nullptr)
            {
                return;
            }

            cout << n->val << " ";
            affichagePrefixePrivate(n->gauche);
            affichagePrefixePrivate(n->droite);
        }

        void affichageSuffixePrivate(noeud* n)// LRD, Left then right then data
        {
            if (n == nullptr)
            {
                return;
            }
            affichageSuffixePrivate(n->gauche);
            affichageSuffixePrivate(n->droite);
            cout << n->val << " ";
        }

        // Teste si l'arbre est un arbre de recherche
        bool estABR(noeud* n)
        {
            if (n == nullptr)
            {
                return true;
            }
            else if (n->gauche->val < n->val && n->val < n->droite->val)
            {
                return true;
            }
            else {
                return false;
            }
            estABR(n->gauche);
            estABR(n->droite);
        }

};

struct noeudAL
{
    int val;
    int* gauche;
    int* droite;
};


class ArbreListe
{
    public:
        ArbreListe()
        {
            arbre = {0,0,0,0};
            size = 0;
        }

        void ajouterElem(int val)
        {
            //element d'indice i
            arbre[size]=val;

            ++size;
            arbre.push_back(0);



        }

        size_t taille() const
        {
            return size;
        }
        //parcours en profondeur
        void affichagePrefixe() const
        {
            cout << arbre[0] << " ";
            for (size_t i(0); 2*i+2 <taille(); ++i)
            {
                cout << arbre[2 * i + 1] << " ";
                cout << arbre[2 * i + 2] << " ";
            }
            if (size%2==0)
            {
                cout<<arbre[size-1];
            }
            cout << endl;
        }
        void affichageInfixe() const
        {
            cout << arbre[0] << " ";
            for (size_t i(0); 2 * i + 2 <taille(); ++i)
            {
                cout << arbre[2 * i + 1] << " ";
                cout << arbre[2 * i + 2] << " ";
            }
            if (size % 2 == 0)
            {
                cout << arbre[size - 1];
            }
            cout << endl;
        }


    private:
        vector<int> arbre;
        size_t size;

};
