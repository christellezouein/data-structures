//Construction d'un arbre binaire de recherche.

#include <iostream>
using namespace std;

struct noeud {
    int val;
    noeud * gauche;// la valeur pointee par le ptr de gauche va etre plus petites que val.
    noeud * droite;// la valeur pointee par le ptr de droite va etre plus grande que val.
};
//et ainsi de suite pour toutes les valeurs de l'arbre.

class Arbre
{
    public:
        Arbre()
        {
            racine = nullptr;
        }

        noeud * creerFeuille(int val) //On pouvait ecrire ce code dans la methode ajouterNoeudPrivate
            //mais pour eviter la duplication de code on cree cette methode qu'on appellera plusieur fois.
        {
            noeud * n = new noeud;
            n->val = val;
            n->gauche = nullptr;
            n->droite = nullptr;

            return n;
        }

        void ajouterFeuille(int val) //n devient la racine, un ptr qui pointera vers le nouveau noeud a ajouter.
        {
            ajouterFeuillePrivate(val, racine);
        }

    private:
        noeud * racine;

        void ajouterFeuillePrivate(int val, noeud* n)
        {
            if (racine == nullptr)
            {
                racine = creerFeuille(val);
            }
            else if (val < n->val)
            {
                racine = n->gauche;
                ajouterFeuillePrivate(val, racine);
            }
            else if (val > n->val)
            {
                racine = n->droite;
                ajouterFeuillePrivate(val, racine);
            }
            else
            {
                cout << "La valeur a deja ete ajoutee a l'arbre ! " << endl;
            }
        }
};
