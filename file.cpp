// Implementation d'un file
// Il serait preferable d'utiliser la queue de C++ pour des raison de perf
// Mais on n'aurait pas appris grand chose sur les files.

class Noeud//noeud pour faire une file de noeud
{
    public:
        noeud* val;
        Noeud* suivant;

};

class File
{
    public:
        File()
        {
            tete = nullptr;
            queue = nullptr;
            ptr = tete;
        }

        void ajouterElem(noeud* n)
        {
            Noeud* nouv = new Noeud;
            nouv->val = n;
            nouv->suivant = nullptr;
            if (tete == nullptr)
            {
                tete = nouv;
                queue = nouv;

            }
            else
            {
                queue->suivant = nouv;
                queue = nouv;
            }
            nouv = nullptr;
            delete nouv;
        }

        noeud* getElem() // le premier element qu'on a saisit
        {
            noeud* n = tete->val;
            tete = tete->suivant;
            return n;

        }

        bool estVide()
        {
            if (tete == nullptr)
            {
                return true;
            }
            return false;
        }


    private:
        Noeud * tete;
        Noeud * queue;
        Noeud * ptr;

};

