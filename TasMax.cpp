#include <iostream>
#include <vector>
#include <utility>

using namespace std;

void maxTasser(vector<int>& tab, int i, int taille) 
{
    int lePlusGrand=i;
    int filsGauche = 2 * i+1;                 //left child
    int filsDroit = 2 * i + 2;          //right child
    if (filsGauche < taille && tab[filsGauche] > tab[i])
    {
        lePlusGrand = filsGauche;
    }

    if (filsDroit < taille && tab[filsDroit] > tab[lePlusGrand])
        lePlusGrand = filsDroit;
    if (lePlusGrand != i)
    {
        swap(tab[i], tab[lePlusGrand]);
        maxTasser(tab, lePlusGrand, taille);
    }
}


void constTasMax(vector<int>& tab)
{
    for (int i = (tab.size() -1)/ 2; i >= 0; --i)
    {
        maxTasser(tab, i, tab.size());
    }
}

void triTas(vector<int>& tab)
{
    int taille = tab.size();
    constTasMax(tab);
    for (int i = tab.size() - 1; i >= 1; --i) // parcours de tt le tab, representant le tas
    {
        swap(tab[0], tab[i]);
        taille = taille - 1; //reduction du tas en decrementant sa taille
        maxTasser(tab, 0, taille);
    }
    //les plus grands elements sont mis a la fin de la liste progressivement
    //et donc la liste est triee dans l'ordre croissant
}

int maximum(vector<int> tab)
{
    if (tab.size() == 0)
    {
        return -1; // Erreur le tableau est vide donc pas de max
    }
    return tab[0]; // la racine est le maximum du tas-max 
}

int popMax(vector<int>& tab) // s'applique a un tas-max 
{
    int taille = tab.size();
    if (taille == 0)
    {
        return -1; // Erreur le tableau est vide donc pas de max
    }
    int maximum = tab[0];
    tab[0] = tab[taille-1];
    maxTasser(tab, 0, taille);
    return maximum;
}

void augmenterValeur(vector<int>& tab, int i, int val)//val est la nouv valeur augmente de tab[i]
{
    if (tab[i] > val)
    {
        cout << "Il faut saisir une val plus grande que celle du noeud" << endl;
        return;
    }

    tab[i] = val;
    while (i>0 && tab[i] > tab[(i - 1) / 2]) //parcours des parent pour voir s'il y en a un plus petit que tab[i] (dont la valeur est aug), alors on les echange.
    {
        swap(tab[(i - 1) / 2], tab[i]);
        i = (i - 1) / 2; 
    }

}

void insererValeur(vector<int>& tab, int val)
{
    tab.push_back(-1); // on considere que tte les val du tas-max sont positives
    augmenterValeur(tab, tab.size()-1, val);
}


int main()
{
    vector<int> tab = {1,14,10,8,7,4,2,3,9,16};
    constTasMax(tab); // O(n log n)
    for (auto i : tab)
    {
        cout << i << endl;
    }

    cout << "-----------------" << endl;

    cout << popMax(tab) << endl;
    cout << popMax(tab) << endl;
    cout << popMax(tab) << endl;
    cout << popMax(tab) << endl;
    cout << popMax(tab) << endl;

    cout << "-----------------" << endl;

    for (auto i : tab)
    {
        cout << i << endl;
    }


    insererValeur(tab, 12);
    cout << "-----------------" << endl;

    for (auto i : tab)
    {
        cout << i << endl;
    }

    return 0;
}
