// C'est un arbre binaire de recherche.

#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
using namespace std;

//  Surcharge de l'operateur << pour afficher les vector plus facilment.
ostream& operator<<(ostream& out, vector<int> t)
{
    out << "[";
    for (auto i : t)
    {
        cout << i << " ";
    }
    out << "]";
    return out;
}


class noeud
{
    public:
        int val;
        noeud * gauche;
        noeud * droite;
        noeud * parent;
};

class Arbre
{
    public:
        Arbre()
        {
            racine = nullptr;
        }



        void ajouterFeuille(int val)
        {
            ajouterFeuillePrivate(val, racine);
        }

        void affichageInfixe()
        {
            affichageInfixePrivate(racine);
        }

        void affichagePrefixe()
        {
            affichagePrefixePrivate(racine);

        }
        void affichageSuffixe()
        {
            affichageSuffixePrivate(racine);

        }

        int taille()
        {
            return taillePrivate(racine);
        }

        int hauteur(int val) // pas de cond sur le fait que la val ne se trouve pas dans l'arbre
        {
            return hauteurPrivate(trouver(val, racine));
        }

        int hauteurArbre()
        {
            return hauteurArbreP(racine);
        }
        void affichageLargeur()
        {
            affichageLargeurPriv(racine);
        }

        bool estDegen()
        {
            return estDegenPrivate(racine);
        }

        bool estComplet()
        {
            return estComp(racine);
        }

        bool estParfait()
        {
            return estParf(racine);
        }
        vector<int> getSerialization()
        {
            serialization(racine);
            return s;
        }

        bool estUnABR()
        {
            return estABR(racine);
        }
        int getMax()
        {
            if (maximum(racine) == nullptr)
            {
                return -47;
            }
            return maximum(racine)->val;
        }

        int getMin()
        {
            if (minimum(racine) == nullptr)
            {
                return -47;
            }
            return minimum(racine)->val;
        }

        int supprimer(int val)
        {
            noeud* n;
            n=supprimerNoeud(racine, val);
            return -1;
        }

        void supprimer2(int val)
        {

            supprimerNoeud2(trouver(val, racine));
        }
        vector<int> ConvEnListeTriee()
        {
            vector<int> t;
            ConvEnListeTrieePriv(racine, t);
            return t;
        }

        void ajouterEnRacine(int val)
        {
            // alloc dynamique des arbres mais ces arbres consistuent une fuite de memoire
            Arbre* ag = new Arbre;
            Arbre* ad = new Arbre;
            // Arbre ag,ad; // alloc stat a condition de changer le destructeur
            couperPriv(racine, val, *ag, *ad);
            racine=creerFeuille(val);

            if (ag->racine != nullptr)
            {
                racine->gauche = ag->racine;
                ag->racine->parent = racine;
            }
            if (ad->racine != nullptr)
            {
                racine->droite = ad->racine;
                ad->racine->parent = racine;
            }

        }

        void couper(int val)
        {
            Arbre ag, ad;
            couperPriv(racine, val, ag, ad);
            cout << "\nArbre de gauche\n";
            ag.affichageInfixe();
            cout << "\nArbre de droite\n";
            ad.affichageInfixe();
            cout << "racine de l'arbre droit apres avoir couper" << ad.racine->val << endl;
            cout << "racine de l'arbre gauche apres avoir couper" << ag.racine->val << endl;

        }

        ~Arbre()
        {
            destroy(racine);
            cout << "Destroying the tree" << endl;
        }

    private:
        noeud * racine;
        vector<int> s;
        vector<int> listetriee;



        void couperPriv(noeud* n, int v, Arbre& ag, Arbre& ad)
        {
            if (n == nullptr)
            {
                return;
            }
            if (n->val < v)
            {
                ag.ajouterFeuille(n->val);
            }
            else
            {
                ad.ajouterFeuille(n->val);
            }

            couperPriv(n->gauche, v, ag, ad);
            couperPriv(n->droite, v, ag, ad);
        }

        void ConvEnListeTrieePriv(noeud* n, vector<int>& l)
        {
            if (n == nullptr)
            {
                return;
            }
            else
            {
                ConvEnListeTrieePriv(n->gauche, l);
                l.push_back(n->val);
                ConvEnListeTrieePriv(n->droite, l);
            }
        }

        // Destruction de l'arbre, construit par alloc dyn.
        void destroy(noeud* n)
        {
            if (n == nullptr)
            {
                return;
            }
            destroy(n->gauche);
            destroy(n->droite);
            delete n;
            n = nullptr;
        }

        // Trouver un noeud de valeur val dans l'arbre.
        noeud* trouver(int val, noeud* n)
        {
            if (val < n->val)
            {
                return trouver(val, n->gauche);
            }
            else if(val > n->val)
            {
                return trouver(val, n->droite);
            }
            else
            {
                return n;
            }
        }

        noeud * creerFeuille(int val) // On pouvait ecrire ce code dans la methode ajouterNoeudPrivate
            // mais pour eviter la duplication de code on creer cette methode, qu'on appellera plusieur fois.
        {
            noeud * n = new noeud;
            n->val = val;
            n->gauche = nullptr;
            n->droite = nullptr;
            n->parent = nullptr;
            return n;
        }
        void ajouterFeuillePrivate(int val, noeud* n)
        {
            noeud * ni = new noeud;
            ni->val = val;
            ni->gauche = nullptr;
            ni->droite = nullptr;
            ni->parent = nullptr;

            if (racine == nullptr)
            {
                // racine = creerFeuille(val); //  Si on veut eviter la dup de code
                racine = ni;
            }
            else if (val < n->val)
            {
                //  Si le ptr gauche n'est pas nul, on le traverse, jusqu'a arriver a ou on veut l'ajouter 
                //  ici, n->gauche est nul, on doit donc le creer.
                //  Dans l'appel recursif de la fct, quand on arrive
                //  a cet endroit, on passera au else, et la creation aura lieu.

                if (n->gauche != nullptr)
                {
                    ajouterFeuillePrivate(val, n->gauche);
                }
                else // S'il est nul, on cree un noeud, dont l'adresse sera mise dans n->gauche
                {
                    // n->gauche = creerFeuille(val); // Si on veut eviter la duplication de code.
                    n->gauche = ni;
                    (n->gauche)->parent = n;
                }
            }
            else if (val>n->val)
            {
                if (n->droite != nullptr)
                {
                    ajouterFeuillePrivate(val, n->droite);
                }
                else
                {
                    // n->droite = creerFeuille(val); // Si on veut eviter la duplication de code.
                    n->droite = ni;
                    (n->droite)->parent = n;
                }
            }
            else
            {
                cout << "La valeur a deja ete ajoutee a l'arbre ! " << endl;
            }
        }

        // Parcours en profondeur
        void affichageInfixePrivate(noeud* n) // LDR, Left then data then right.
        {

            if (n == nullptr)
            {
                return;
            }
            affichageInfixePrivate(n->gauche);
            cout << n->val << " ";
            affichageInfixePrivate(n->droite);
        }


        void affichagePrefixePrivate(noeud* n) // DLR, Data then left then right
        {
            if (n == nullptr)
            {
                return;
            }
            cout << n->val << " ";
            affichagePrefixePrivate(n->gauche);
            affichagePrefixePrivate(n->droite);
        }

        void affichageSuffixePrivate(noeud* n)//  LRD, Left then right then data
        {
            if (n == nullptr)
            {
                return;
            }
            affichageSuffixePrivate(n->gauche);
            affichageSuffixePrivate(n->droite);
            cout << n->val << " ";
        }

        // Taille de l'arbre.
        int taillePrivate(noeud* n) const
        {

            if (n == nullptr)
            {
                return 0;
            }
            return 1 + taillePrivate(n->gauche) + taillePrivate(n->droite);
        }

        // Hauteur de l'arbre a partir du parent.
        int hauteurPrivate(noeud* n) const
        {
            if (n == racine)
            {
                return 0;
            }
            return 1 + hauteurPrivate(n->parent);
        }

        // Hauteur d'un noeudi quelconque.
        int hauteurArbreP(noeud* n)
        {
            if (n == nullptr)
            {
                return -1;
            }

            return 1 + max(hauteurArbreP(n->gauche), hauteurArbreP(n->droite));
        }

        // parcours en largeur
        void affichageLargeurPriv(noeud* n)
        {
            if (n == nullptr)
            {
                return;
            }
            File f;
            // queue<noeud*> q; // Si l'on veut une meilleur perf.
            f.ajouterElem(n);
            // q.push(n);
            while (!f.estVide()) // q.empty()
            {
                noeud* N;
                N = f.getElem();
                // N = q.front();
                cout << N->val << " ";
                // q.pop();
                if (N->gauche != nullptr)
                {
                    f.ajouterElem(N->gauche);
                    // q.push(N->gauche);
                }
                if (N->droite != nullptr)
                {
                    f.ajouterElem((N)->droite);
                    // q.push(N->droite);
                }
            }
            cout << endl;
        }

        // Teste si l'arbre est degenere.
        bool estDegenPrivate(noeud* n)
        {
            if (n == nullptr)
            {
                return true;
            }

            if (n->gauche != nullptr && n->droite == nullptr)
            {
                return true;
            }
            else if (n->gauche == nullptr && n->droite != nullptr) 
            {
                return true;
            }
            else
            {
                return false;
            }
            estDegenPrivate(n->gauche);
            estDegenPrivate(n->droite);
        }

        // Teste si l'arbre est complet.
        bool estComp(noeud* n)
        {
            if (n == nullptr) { return true; }
            if (n->gauche == nullptr && n->droite == nullptr)
            {
                return true;
            }

            else if (n->gauche != nullptr && n->droite != nullptr)
            {
                estComp(n->gauche);
                estComp(n->droite);
            }
            else
            {
                return false;
            }
        }

        // Teste si l'arbre est complet avec un parcours recursif a la fin.
        bool estComp1(noeud* n)
        {
            if (n == nullptr) { return true; }
            if ((n->gauche == nullptr && n->droite != nullptr)
                    || (n->gauche != nullptr && n->droite == nullptr))
            {
                return false;
            }
            else if (n->gauche == nullptr && n->droite == nullptr)
            {
                return true;
            }
            else
            {
                estComp1(n->gauche);
                estComp1(n->droite);
            }
        }

        // Teste si l'arbre est parfait.
        bool estParf(noeud* n)
        {
            if (n == nullptr) { return true; }
            if (n->gauche == nullptr && n->droite != nullptr) {
                return false;
            }

            else if (n->gauche != nullptr && n->droite != nullptr) {
                estParf(n->gauche);
                estParf(n->droite);
            }
            else {
                return true;
            }
        }

        // Serialisation d'un arbre en une liste.
        void serialization(noeud* n)
        {
            if (n == nullptr)
            {
                s.push_back(0);
                return;
            }

            s.push_back(n->val);
            serialization(n->gauche);
            serialization(n->droite);
        }

        // Teste si l'arbre est un arbre binaire de recherche.
        bool estABR(noeud* n)
        {
            if (n == nullptr)
            {
                return true;
            }
            if ((n->gauche)->val < n->val && n->val < (n->droite)->val)
            {
                return true;
            }
            else{
                return false;
            }
            estABR(n->gauche);
            estABR(n->droite);
        }

        // Trouve l'elt maximal.
        noeud* maximum(noeud* n) // le max va tjs etre dans la branche droite ou le sous arbre droit
        {
            if (n == nullptr)
            {
                return n;
            }
            while (n->droite!=nullptr)// si n->droite est nullptr, donc la racine du sous-arbre est le plus grand element
            {
                n = n->droite;

            }
            return n;
        }

        // Trouve l'elt minimal.
        noeud* minimum(noeud* n) // le min va tjs etre dans la branche gauche ou le sous arbre gauche
        {
            if (n == nullptr)
            {
                return n;
            }
            while (n->gauche != nullptr) // si n->gauche est nullptr, donc la racine du sousarbre est le plus petit element
            {
                n = n->gauche;

            }
            return n;
        }

        // Supprimer un noeud de l'arbre sans corrompre l'arbre
        noeud* supprimerNoeud(noeud* n, int val)
        {
            if (n == nullptr)
            {
                return n;
            }
            else if (val < n->val)
            {
                n->gauche = supprimerNoeud(n->gauche, val);
                // dans n->gauche on met la le ptr apres l'elt supprime
                // quand c'est nullptr, le ptr gauche ou droite de la feuille devient = a nullptr,
                // ce (n) n'est pas juste deleted et n->droite ou gauche n'est plus nullptr
                // et on ne peut plus parc. On remedie a ce porb.
            }
            else if(val>n->val)
            {
                n->droite = supprimerNoeud(n->droite, val);
            }
            if (val==n->val)
            {
                if (n->gauche == nullptr && n->droite == nullptr)
                {
                    delete n;
                    n = nullptr;
                    return n;
                }
                else if (n->gauche == nullptr && n->droite != nullptr)
                {
                    noeud * temp = n;
                    n = n->droite;
                    cout << temp->val << "   " << n->val<<endl;
                    delete temp;
                    temp = nullptr;
                    return n;
                }
                else if (n->gauche != nullptr && n->droite == nullptr)
                {
                    noeud * temp = n;
                    n = n->gauche;
                    cout << temp->val << "   " << n->val << endl;
                    delete temp;
                    temp = nullptr;
                    return n;
                }
                else if (n->gauche != nullptr && n->droite != nullptr)
                {
                    noeud* p = minimum(n->droite);
                    n->val = p->val;
                    n->droite = supprimerNoeud(n->droite, p->val);
                }
            }

        }

        // Suppression en utilisant la notion de parent.
        void supprimerNoeud2(noeud* n)
        {
            if (n->gauche == nullptr && n->droite == nullptr)
            {
                if (n == n->parent->gauche)
                {
                    n->parent->gauche = nullptr;
                }
                else
                {
                    n->parent->droite = nullptr;
                }

                delete n;
                n = nullptr;
            }
            else if (n->gauche == nullptr && n->droite != nullptr)
            {
                n->parent->droite = n->droite;
                n->droite->parent = n->parent;
                delete n;
                n = nullptr;
            }
            else if (n->gauche != nullptr && n->droite == nullptr)
            {
                n->parent->gauche = n->gauche;
                n->gauche->parent = n->parent;
                delete n;
                n = nullptr;
            }
            else if (n->gauche != nullptr && n->droite != nullptr)
            {
                noeud* p = minimum(n->droite);
                n->val = p->val;
                supprimerNoeud2(p);

            }

        }

};
